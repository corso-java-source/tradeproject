package it.stefanobizzi.corsi.javase;

import java.util.Scanner;

public class ScannerSample {
    public static void main(String[] args) {
        // java.util - https://docs.oracle.com/javase/7/docs/api/java/util/Scanner.html
        Scanner scanner = new Scanner(System.in);
        System.out.print("Scrivi un nome: ");
        String nome = scanner.next();
        System.out.println("Ciao " + nome);

        System.out.print("Inserisci un importo: ");
        float myFloatVar = scanner.nextFloat();
        System.out.println("Importo :" + myFloatVar);
    }
}
