package it.stefanobizzi.corsi.javase;

public class UserAccount {
    String name;
    float balance;

    public UserAccount(final String name) {
        this.name = name;
    }

    public UserAccount(final String name, final float initialBalance) {
        this.name = name;
        this.balance = (initialBalance >= 0) ? initialBalance : 0.0F;
    }

    public float getBalance() {
        return this.balance;
    }

    public String rechargeAmount(final float chargeAmount) {
        String result = null;
        if (chargeAmount > 0) {
            this.balance += chargeAmount;
        } else {
            result = "Cannot charge a negative amount!";
        }
        return result;
    }

    public String payAmount(final float paymentAmount) {
        if (paymentAmount > 0) {
            if (paymentAmount > this.balance) {
                return "You don't have enough money!";
            }
            this.balance -= paymentAmount;
            return null;
        }
        return "Cannot pay a negative amount!";
    }



























}
