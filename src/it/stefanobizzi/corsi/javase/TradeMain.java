package it.stefanobizzi.corsi.javase;

import java.util.Scanner;

public class TradeMain {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Account name: ");
        String accountName = scanner.next();
        System.out.print("Initial Balance: ");
        float initialBalance = scanner.nextFloat();
        UserAccount userAccount = new UserAccount(accountName, initialBalance);
        System.out.println("Account name: " + userAccount.name);
        System.out.println("Initial balance: " + userAccount.getBalance());

        // Richieda un importo di ricarica e lo applichi allo userAccount
        System.out.print("Recharge amount: ");
        float rechargeAmount = scanner.nextFloat();
        String rechargeResult = userAccount.rechargeAmount(rechargeAmount);
        if (rechargeResult != null) {
            System.out.println("ERROR! " + rechargeResult);
        }
        System.out.println("Current balance: " + userAccount.getBalance());
        // Richieda un importo di spesa e lo applichi allo userAccount
        System.out.print("Payment amount: ");
        float paymentAmount = scanner.nextFloat();
        String paymentResult = userAccount.payAmount(paymentAmount);
        if (paymentResult != null) {
            System.out.println("ERROR! " + paymentResult);
        }
        System.out.println("Current balance: " + userAccount.getBalance());
    }
}

























